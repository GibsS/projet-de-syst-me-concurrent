# Librairie de gestion d'objet partagés #

### Sommaire: ###

* Cette librairie offre la possibilité de partager des objets java entre plusieurs machines sans avoir à se soucier de la logistique de communication ou de gestion de concurrence.
* Version : 1.0

### Contacts : ###

* Emerick Gibson : emerick.gibson@hotmail.fr