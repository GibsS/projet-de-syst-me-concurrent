import java.rmi.RemoteException;

public class Stresseur {
	private static final int NUMBER_OF_THREADS = 100;

	
	public static void main(String[] args){
		Writers[] ws = new Writers[10];
		//Open the server
		try {
			new Server(5000);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		Client.init();
		// look up the IRC object in the name server
		// if not found, create it, and register it in the name server
		SharedObject s = Client.lookup("IRC");
		if (s == null) {
			s = Client.create(new TestSentence());
			Client.register("IRC", s);
		}
		
		for(int i = 0;i < NUMBER_OF_THREADS;i ++) {
			ws[i] = new Writers(s);
		}
		
		for(int i = 0;i < NUMBER_OF_THREADS;i ++)
			ws[i].start();
		
	}
}
