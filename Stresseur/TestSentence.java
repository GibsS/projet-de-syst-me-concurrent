@SuppressWarnings("serial")
public class TestSentence implements java.io.Serializable {
	int data;
	public TestSentence() {
		this.data = 0;
	}
	
	public void write(int i,int operation) {
		if(operation == 0) data += i;
		else data -= i;
	}
	public int read() {
		return this.data;	
	}
	
}
