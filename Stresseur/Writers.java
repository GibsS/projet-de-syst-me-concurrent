import java.util.Random;

public class Writers extends Thread {
	public SharedObject s;
	
	public Writers(SharedObject s){
		this.s = s;
	}
	
	public void run(){
		Random rn = new Random();
		int value = rn.nextInt(1000);
		int operation = rn.nextInt(2);
		// lock the object in write mode
		this.s.lock_write();
		((TestSentence)(this.s.obj)).write(value,operation);
		this.s.unlock();
	}

}
