import java.io.*;

@SuppressWarnings("serial")
public class SharedObject implements Serializable, SharedObject_itf {
	
	public Object obj;
	
	int id;
	
	LockStatus lock;
	
	public SharedObject(int i) {
		id = i;
		setLock(LockStatus.NL);
	}
	
	public int getId() {
		return id;
	}
	
	public void setLock(LockStatus l) {
		lock = l;
	}
	
	// invoked by the user program on the client node
	public synchronized void lock_read() {
		switch(lock) {
		case NL : obj = Client.lock_read(id);
		case RLC : 
			setLock(LockStatus.RLT); 
			break;
		case WLC : 
			setLock(LockStatus.RLT_WLC); 
			break;
		case WLT : 
			// impossible
			break;
		case RLT_WLC :
		case RLT : break;
		}
	}

	// invoked by the user program on the client node
	public synchronized void lock_write() {
		switch(lock) {
			case NL : 
			case RLT :
			case RLC : obj = Client.lock_write(id);
			case RLT_WLC :
			case WLC : setLock(LockStatus.WLT); break;
			case WLT : break;
		}
	}

	// invoked by the user program on the client node
	public synchronized void unlock() {
		switch(lock) {
			case NL : 
			case RLC : 
			case WLC : break;
			case RLT : 
				setLock(LockStatus.RLC); 
				this.notifyAll();
				break;
			case WLT : 
				setLock(LockStatus.WLC); 
				this.notifyAll(); 
				break;
			case RLT_WLC : 
				setLock(LockStatus.WLC); 
				this.notifyAll();
				break;
		}
	}

	// callback invoked remotely by the server : signifie qu'une personne veut lire
	public synchronized Object reduce_lock() {
		try {
			switch(lock) {
				case NL : return null; 
				case RLC : 
				case RLT : break;
				case WLT : 
					this.wait();
					setLock(LockStatus.RLC);
					break;
				case WLC : 
					setLock(LockStatus.RLC); 
					break;
				case RLT_WLC : 
					setLock(LockStatus.RLT); 
					break;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	// callback invoked remotely by the server
	public synchronized void invalidate_reader() {
		try {
			switch(lock) {
				case RLT_WLC :
				case RLT : 
					this.wait();
					break;
				case NL : 
				case RLC : 
				case WLT : 
				case WLC : break; 
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		setLock(LockStatus.NL); 
	}

	public synchronized Object invalidate_writer() {
		try {
			switch(lock) {
				case RLT_WLC :
				case WLT : 
					this.wait();
					break;
				case RLT : 
				case NL :  
				case RLC :
				case WLC : break;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		setLock(LockStatus.NL);
		return obj;
	}
	
	public synchronized void invalidate_modification() {
		setLock(LockStatus.NL);
	}
}
