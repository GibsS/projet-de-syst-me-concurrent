import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings("serial")
public class TestGUI extends Frame {
	Label etatLock;
	TextField donnee;
	Button lockReadButton;
	Button lockWriteButton;
	Button unlockButton;
	
	public LockStatus lockPrecedent;
	public SharedObject so;
	
	public static void main(String[] args) {
		// initialize the system
		Client.init();
		
		// look up the IRC object in the name server
		// if not found, create it, and register it in the name server
		SharedObject s = Client.lookup("IRC");
		if (s == null) {
			s = Client.create(new Donnee());
			Client.register("IRC", s);
		}
		// create the graphical part
		new TestGUI(s);
	}
	
	@SuppressWarnings("deprecation")
	public TestGUI(SharedObject s) {
		so = s;
		
		setLayout(new GridLayout(5, 1));
	
		etatLock = new Label();
		etatLock.setText("NL");
		add(etatLock);
	
		donnee = new TextField();
		add(donnee);
	
		lockReadButton = new Button("lock read");
		lockReadButton.addActionListener(new lockReadListener());
		add(lockReadButton);
		lockWriteButton = new Button("lock write");
		lockWriteButton.addActionListener(new lockWriteListener());
		add(lockWriteButton);
		unlockButton = new Button("unlock");
		unlockButton.addActionListener(new unlockListener());
		add(unlockButton);
		
		setSize(300,200);
		show();
		
		lockPrecedent = LockStatus.NL;
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if(lockPrecedent != so.lock) {
					donnee.setText(Integer.toString(((Donnee)so.obj).donnee));
					lockPrecedent = so.lock;
				}
				switch(so.lock) {
				case NL : etatLock.setText("NL");break;
				case RLC : etatLock.setText("RLC");break;
				case WLC : etatLock.setText("WLC");break;
				case WLT : etatLock.setText("WLT");break;
				case RLT_WLC : etatLock.setText("RLT_WLC");break;
				case RLT : etatLock.setText("RLT");break;
				}
			}
		}, 0, 300);
	}
	
	class lockReadListener implements ActionListener {
		public void actionPerformed (ActionEvent e) {
			if(so.lock == LockStatus.WLT)
				((Donnee)so.obj).donnee = Integer.parseInt(donnee.getText());
			so.lock_read();
		}
	}
	class lockWriteListener implements ActionListener {
		public void actionPerformed (ActionEvent e) {
			so.lock_write();
		}
	}
	class unlockListener implements ActionListener {
		public void actionPerformed (ActionEvent e) {
			if(so.lock == LockStatus.WLT)
				((Donnee)so.obj).donnee = Integer.parseInt(donnee.getText());
			so.unlock();
		}
	}
}

@SuppressWarnings("serial")
class Donnee implements Serializable {
	public int donnee;
	
	public int getDonnee() {
		return donnee;
	}
	public void setDonnee(int d) {
		donnee = d;
	}
}