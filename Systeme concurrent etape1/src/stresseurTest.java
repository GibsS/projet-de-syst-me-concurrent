import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.rmi.RemoteException;
import java.util.Random;

public class stresseurTest {
	
	SharedObject		sharedInt;

	public static void main(String argv[]) throws InterruptedException, IOException {
		
		Client.init();
		
		// look up the IRC object in the name server
		// if not found, create it, and register it in the name server
		SharedObject s = Client.lookup("SharedInt");
		if (s == null) {
			s = Client.create(new sharedInteger());
			Client.register("SharedInt", s);
			Files.write(Paths.get("stresseurResults.txt"), "".getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
		}
		
		stresseurTest stresseur = new stresseurTest(s);	
		
		
		for (int i = 0; i < 400; i++) {
			stresseur.sharedInt.lock_write();
			((sharedInteger)(stresseur.sharedInt.obj)).write();
			stresseur.sharedInt.unlock();
		}
		
		Thread.sleep(5000); 
		
		stresseur.sharedInt.lock_read();
		System.out.println(((sharedInteger)(stresseur.sharedInt.obj)).read());
		String st = (((sharedInteger)(stresseur.sharedInt.obj)).read())+"\n";
		Files.write(Paths.get("stresseurResults.txt"), st.getBytes(), StandardOpenOption.APPEND);
		stresseur.sharedInt.unlock();
		
		
	}
	
	public stresseurTest(SharedObject s) {
		sharedInt = s;
	}
		
		
		
}