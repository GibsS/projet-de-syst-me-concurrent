@SuppressWarnings("serial")
public class Sentence implements java.io.Serializable {
	String data;
	public Sentence() {
		data = new String("");
	}
	
	public void write(String text) {
		data += text + "\n";
	}
	public String read() {
		return data;	
	}
	
}