import java.util.ArrayList;
import java.util.List;

public class Transaction {
	
	static Transaction instance;
	boolean active;
	
	List<Integer> idModifie;
	
	public Transaction() {
		idModifie = new ArrayList<Integer>();
	}	
	
	public static Transaction getCurrentTransaction() {
		return instance;
	}

	// indique si l'appelant est en mode transactionnel
	public boolean isActive() {
		return active;
	}
	
	// demarre une transaction (passe en mode transactionnel)
	public void start() {
		Client.modeTransaction.lock();
		if(instance == null || instance.isActive()) {
			instance = this;
			active = true;
			idModifie.clear();
		} else {
			System.out.println("Une transaction est encore en cours");
		}
		Client.modeTransaction.unlock();
	}
	
	// termine une transaction et passe en mode non transactionnel
	public boolean commit(){
		Client.modeTransaction.lock();
		active = false;
		Client.condition.signalAll();
		Client.modeTransaction.unlock();
		return true;
	}
		
	// abandonne et annule une transaction (et passe en mode non transactionnel)
	public void abort(){
		Client.modeTransaction.lock();

		for(int id : idModifie)
			Client.invalidate_modification(id);
		
		active = false;
		Client.condition.signalAll();
		Client.modeTransaction.unlock();
	}
}