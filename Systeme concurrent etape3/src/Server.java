import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Server extends UnicastRemoteObject implements Server_itf {

	// clients
	public List<Client_itf> clients;
	
	//objets partag�s
	public Map<Integer, Object> objects;
	public Map<Integer, List<Client_itf>> readLockOwner;
	public Map<Integer, Client_itf> writeLockOwner;
	
	// gestion d'ID
	public int prochainID;
	public Map<String, Integer> nameToId;
	public Map<Integer, String> types;
	
	public static void main(String[] args) {
		try {
			new Server(5000);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public Server(int port) throws RemoteException {
		clients = new ArrayList<Client_itf>();
		
		objects = new HashMap<Integer, Object>();
		readLockOwner = new HashMap<Integer, List<Client_itf>>();
		writeLockOwner = new HashMap<Integer, Client_itf>();
		
		prochainID = 0;
		nameToId = new HashMap<String, Integer>();
		types = new HashMap<Integer, String>();
		
		try {
			LocateRegistry.createRegistry(port);
			Naming.rebind("//localhost:" + port + "/serveur", this);
			System.out.println("Serveur launched at : " + "//localhost:" + port + "/serveur");
		} catch (RemoteException | MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public int getNouveauId() {
		return prochainID++;
	}
	
	public int lookup(String name) throws RemoteException {
		Integer i;
		return ((i = nameToId.get(name)) == null) ? -1:i;
	}
	public String getType(int id) throws RemoteException {
		return types.get(id);
	}

	public int create(Object o) throws RemoteException {
		int id = getNouveauId();
		types.put(id, o.getClass().getName());
		objects.put(id, o);
		readLockOwner.put(id, new ArrayList<Client_itf>());
		writeLockOwner.put(id, null);
		return id;
	}
	
	public void register(String name, int o) throws RemoteException {
		nameToId.put(name, o);
	}

	public synchronized Object lock_read(int id, Client_itf client) throws RemoteException {
		// R�duire le droit d'�criture si quelqu'un l'a
		if(writeLockOwner.get(id) != null) {
			Object object = writeLockOwner.get(id).reduce_lock(id);
			if(object != null) {
				objects.put(id, object);
			}
			// ne pas oublier de mettre l'�crivain en lecteur
			readLockOwner.get(id).add(writeLockOwner.get(id));
			writeLockOwner.put(id, null);
		}
			
		// Donner le droit de lecture
		readLockOwner.get(id).add(client);

		return objects.get(id);
	}

	public synchronized Object lock_write(int id, Client_itf client) throws RemoteException {
		// Retirer les droits d'�critures
		if(writeLockOwner.get(id) != null) {
			Object object = writeLockOwner.get(id).invalidate_writer(id);
			if(object != null) {
				objects.put(id, object);
			}
			writeLockOwner.put(id, null);
		}
		// Retirer les droits de lecture
		for(Client_itf c : readLockOwner.get(id)) {
			if(!c.equals(client))
				c.invalidate_reader(id);
		}
		readLockOwner.get(id).clear();
			
		// Donner le droit d'�criture
		writeLockOwner.put(id, client);
		
		return objects.get(id);
	}
}