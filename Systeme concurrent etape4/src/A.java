@SuppressWarnings("serial")
public class A implements java.io.Serializable {
	String nom;
	B_itf obj;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public B_itf getB() {
		return obj;
	}
	public void setB(B_itf b) {
		this.obj = b;
	}
}