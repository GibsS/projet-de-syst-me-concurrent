public interface A_itf extends SharedObject_itf {
	String getNom();
	void setNom(String nom);
	B_itf getB();
	void setB(B_itf b);
}