@SuppressWarnings("serial")
public class A_stub extends SharedObject implements A_itf {
	public A_stub(int i) {
		super(i);
	}
	public B_itf getB(){
		B_itf res = ((A)obj).getB();
		return res;
	}
	public java.lang.String getNom(){
		java.lang.String res = ((A)obj).getNom();
		return res;
	}
	public void setNom(java.lang.String arg0){
		((A)obj).setNom(arg0);
	}
	public void setB(B_itf arg0){
		((A)obj).setB(arg0);
	}
}