public class B_stub extends SharedObject implements B_itf {
	public B_stub(int i) {
		super(i);
	}
	public java.lang.String getNom(){
		LockStatus preLock = this.lock;
		java.lang.String res = ((B)obj).getNom();
		return res;
	}
	public void setNom(java.lang.String arg0){
		LockStatus preLock = this.lock;
		((B)obj).setNom(arg0);
	}
}