import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.net.*;

@SuppressWarnings("serial")
public class Client extends UnicastRemoteObject implements Client_itf {
	
	public static Client client;
	
	static Server_itf serveur;
	
	static Map<SharedObject, Integer> ids;
	static Map<Integer, SharedObject> objects;
	
	static Lock modeTransaction;
	static Condition condition;	
	
	static StubGenerator stubGenerator;
	
	private Client(Server_itf s) throws RemoteException {
		super();
		serveur = s;
	}

///////////////////////////////////////////////////
//         Interface to be used by applications
///////////////////////////////////////////////////

	// initialization of the client layer
	public static void init() {
		try {
			serveur = (Server_itf) Naming.lookup("//localhost:5000/serveur");
			client = new Client(serveur);
			
			ids = new HashMap<SharedObject, Integer>();
			objects = new HashMap<Integer, SharedObject>();

			modeTransaction = new ReentrantLock();
			condition = modeTransaction.newCondition();
			
			stubGenerator = new StubGenerator();
		} catch (RemoteException | MalformedURLException |NotBoundException e) {
			e.printStackTrace();
		}
	}
	
	// lookup in the name server
	public static SharedObject lookup(String name) {
		int id;	
		String type;
		SharedObject s = null;
		try {
			id = serveur.lookup(name);
			type = serveur.getType(id);
			if(id != -1) {
				s = stubGenerator.getStub(type, id);
				ids.put(s, id);
				objects.put(id, s);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return s;
	}	
	// n�cessaire pour le read resolve du  sharedobject : cr�� une r�f�rence � un objet partag� en connaissant 
	// rien que l'identifiant
	public static SharedObject lookupId(int id) {
		try {
			//System.out.println("lookup : " + id);
			String type = serveur.getType(id);
			if(type != null) {
				SharedObject so = stubGenerator.getStub(type, id);
				ids.put(so, id);
				objects.put(id, so);
				return so;
			} else {
				return null;
			}
		} catch(RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static SharedObject lookupInternal(int i) {
		return objects.get(i);
	}
	
	// binding in the name server
	public static void register(String name, SharedObject_itf so) {
		try {
			serveur.register(name, ids.get(so));
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	// creation of a shared object
	public static SharedObject create(Object o) {
		try {	
			int id = serveur.create(o);
			SharedObject so = stubGenerator.getStub(o, id);
			ids.put(so, id);
			objects.put(id, so);
			return so;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}
	
/////////////////////////////////////////////////////////////
//    Interface to be used by the consistency protocol
////////////////////////////////////////////////////////////

	// request a read lock from the server
	public static Object lock_read(int id) {
		try {
			Object o = serveur.lock_read(id, client);
			if(Transaction.instance != null && Transaction.instance.isActive()) {
				Transaction.instance.idModifie.add(id);
			}
			return o;
		} catch (RemoteException e) {
				e.printStackTrace();
		}
		return null;
	}

	// request a write lock from the server
	public static Object lock_write (int id) {
		try {
			Object o = serveur.lock_write(id, client);
			if(Transaction.instance != null && Transaction.instance.isActive()) {
				Transaction.instance.idModifie.add(id);
			}
			return o;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	// receive a lock reduction request from the server
	public Object reduce_lock (int id) throws java.rmi.RemoteException {
		try {
			modeTransaction.lock();		
			if(Transaction.instance != null && Transaction.instance.isActive()) {
				condition.await();
			}
			modeTransaction.unlock();
		
			if(objects.containsKey(id))
				return objects.get(id).reduce_lock();
			else
				return null;
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	// receive a reader invalidation request from the server
	public void invalidate_reader(int id) throws java.rmi.RemoteException {
		try {
			modeTransaction.lock();	
			if(Transaction.instance != null && Transaction.instance.isActive()) {
				condition.await();
			}
			modeTransaction.unlock();

			if(objects.containsKey(id))
				objects.get(id).invalidate_reader();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	

	// receive a writer invalidation request from the server
	public Object invalidate_writer(int id) throws java.rmi.RemoteException {
		try {
			modeTransaction.lock();
			if(Transaction.instance != null && Transaction.instance.isActive()) {
				condition.await();
			}
			modeTransaction.unlock();
		
			if(objects.containsKey(id))
				return objects.get(id).invalidate_writer();
			else
				return null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void invalidate_modification(int id) {
		if(objects.containsKey(id))
			objects.get(id).invalidate_modification();
	}
}
