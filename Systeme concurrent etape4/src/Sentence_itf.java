public interface Sentence_itf extends SharedObject_itf {
	@write
	public void write(String text);
	@read
	public String read();
}