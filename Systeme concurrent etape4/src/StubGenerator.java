import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

public class StubGenerator {
	
	Map<String, Class<?>> stubs;
	
	public StubGenerator() {
		stubs = new HashMap<String, Class<?>>();
	}
	
	public static void main(String[] args) {
		StubGenerator sg = new StubGenerator();
		Sentence s = new Sentence();
		Sentence_itf si = (Sentence_itf)sg.getStub(s, 4);
		System.out.println(si + " " + ((SharedObject)si).getId());
		System.out.println(new StubGenerator().generateStubCode("Sentence"));
	}
	
	public SharedObject getStub(Object o, int id) {
		return getStub(o.getClass().getName(), id);
	}
	
	@SuppressWarnings({"rawtypes" })
	public SharedObject getStub(String nom, int id) {
		try {
			Class<?> cls = null;
			try {
				cls = Class.forName(nom + "_stub");
			} catch(ClassNotFoundException e) {
				
			}
			if(cls == null) {
				cls = stubs.get(nom);
			}
			if(cls == null) {
				cls = generateStubClass(nom);
			}
			Class[] argtypes = {int.class};
			return (SharedObject) cls.getDeclaredConstructor(argtypes).newInstance(new Integer(id));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("resource")
	public Class<?> generateStubClass(String nom) {
		try {
			// Prepare source somehow.
			String source = generateStubCode(nom);

			// Save source in .java file.
			File root = new File("stubs"); // On Windows running on C:\, this is C:\java.
			File sourceFile = new File(root, nom + "_stub.java");
			sourceFile.getParentFile().mkdirs();
			new FileWriter(sourceFile).append(source).close();

			// Compile source file.
			JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
			System.out.println(sourceFile.getPath() + " " + compiler);
			compiler.run(null, null, null, sourceFile.getPath());

			// Load and instantiate compiled class.
			URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { root.toURI().toURL() });
			Class<?> cls = Class.forName(nom + "_stub", true, classLoader);
			stubs.put(nom, cls);
			return cls;
			//Object instance = cls.newInstance(); // Should print "world".
			//System.out.println(instance); // Should print "test.Test@hashcode".
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String generateStubCode(String nom) {
		StringBuilder sb = new StringBuilder();
		String nomStub = nom + "_stub";
		String nomItf = nom + "_itf";
		Class<?> itf = null;
		try {
			itf = Class.forName(nomItf);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		// 1. cr�ation de l'ent�te
		sb.append("public class " + nomStub + " extends SharedObject implements " + nomItf + " {\n");

		// 2. cr�ation du constructeur
		sb.append("\tpublic " + nomStub + "(int i) {\n");
		sb.append("\t\tsuper(i);\n");
		sb.append("\t}\n");
		
		// 3. cr�ation des m�thodes
		for(Method m : itf.getMethods()){
			if(!m.getName().equals("lock") && !m.getName().equals("unlock") 
					&& !m.getName().equals("lock_read")
					&& !m.getName().equals("lock_write")) {
				
				sb.append("\t");
				// 3.1. Modifiers
				sb.append("public ");
				// 3.2. Type de retour 
				sb.append(m.getReturnType().getName());
				// 3.3. Nom de la m�thode
				sb.append(" " + m.getName() + "(");
				Parameter[] parameters = m.getParameters();
				if(parameters.length > 0) {
					sb.append(parameters[0].getType().getName() + " " + parameters[0].getName());
					
					for(int i = 1; i < parameters.length; i++){
						sb.append(", " + parameters[i].getType().getName() + " " + parameters[i].getName());
					}
				}
				sb.append(")");
				
				sb.append("{\n");
				// 3.4. appel du lock si n�cessaire
				sb.append("\t\tLockStatus preLock = this.lock;\n");
				if(m.getAnnotation(write.class) != null) {
					sb.append("\t\tthis.lock_write();\n");
				} else if(m.getAnnotation(read.class) != null) {
					sb.append("\t\tthis.lock_read();\n");
				}
				
				// 3.5. appel de la m�thode sous jacente
				sb.append("\t\t");
				if(!m.getReturnType().equals(Void.TYPE)) {
					//sb.append("return ");
					sb.append(m.getReturnType().getName() + " res = ");
				}
				sb.append("((" + nom + ")obj)."+m.getName() + "(");
				if(parameters.length > 0) {
					sb.append(parameters[0].getName());
					for(int i = 1; i < parameters.length; i++){
						sb.append(", " + parameters[i].getName());
					}
				}
				sb.append(");\n");
				
				// 3.6 s'assurer que les locks sont coh�rents
				if(m.getAnnotation(write.class) != null || m.getAnnotation(read.class) != null) {
					sb.append("\t\tswitch(preLock) {\n");
					sb.append("\t\t\tcase WLT : this.lock_write(); break;\n");
					sb.append("\t\t\tcase RLT : this.lock_read(); break;\n");
					sb.append("\t\t\tdefault : this.unlock(); break;\n");
					sb.append("\t\t}\n");
				}
				
				// 3.7 retourner une valeur si n�cessaire
				if(!m.getReturnType().equals(Void.TYPE)) {
					sb.append("\t\treturn res;\n");
					//sb.append(m.getReturnType().getName() + " res = ");
				}
				
				sb.append("\t}\n");
			}
		}
		
		sb.append("}");
		return sb.toString();
	}
}
