public class Test {

	public static void main(String[] args) {
		Client.init();
		
		System.out.println("debut du test");
		A_itf a = (A_itf)Client.lookup("a");
		B_itf b;
		if (a == null) {
			System.out.println("cr�ation de A et B");
			a = (A_itf)Client.create(new A());
			b = (B_itf)Client.create(new B());
			Client.register("a", a);
			
			b.lock_write();
			b.setNom("testB");
			b.unlock();
			
			a.lock_write();
			a.setB(b);
			a.setNom("testA");
			a.unlock();
			System.out.println("A et B sont cr��s");
		} else {
			System.out.println("lecture de A et B : " + a);
			a.lock_read();
			System.out.println(a.getNom());
			
			b = a.getB();
			b.lock_read();
			System.out.println(b.getNom());
			b.unlock();
			
			a.unlock();
			System.out.println("fin de lecture de A et B");
		}
	}
}